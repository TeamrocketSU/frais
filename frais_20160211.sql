-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: frais
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `temp_filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etat_fiche`
--

DROP TABLE IF EXISTS `etat_fiche`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etat_fiche` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etat_fiche`
--

LOCK TABLES `etat_fiche` WRITE;
/*!40000 ALTER TABLE `etat_fiche` DISABLE KEYS */;
INSERT INTO `etat_fiche` VALUES (1,'Fiche crée, saisie en cours'),(2,'Saisie clôturée'),(3,'Validée, mise en paiement'),(4,'Refusée');
/*!40000 ALTER TABLE `etat_fiche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etat_ligne`
--

DROP TABLE IF EXISTS `etat_ligne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etat_ligne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etat_ligne`
--

LOCK TABLES `etat_ligne` WRITE;
/*!40000 ALTER TABLE `etat_ligne` DISABLE KEYS */;
/*!40000 ALTER TABLE `etat_ligne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fiche_frais`
--

DROP TABLE IF EXISTS `fiche_frais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fiche_frais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etat` int(11) DEFAULT '1',
  `date` datetime NOT NULL,
  `date_modification` datetime DEFAULT NULL,
  `utilisateur` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5FC0A6A79B80EC64` (`utilisateur`),
  KEY `IDX_5FC0A6A755CAF762` (`etat`),
  CONSTRAINT `FK_5FC0A6A755CAF762` FOREIGN KEY (`etat`) REFERENCES `etat_fiche` (`id`),
  CONSTRAINT `FK_5FC0A6A79B80EC64` FOREIGN KEY (`utilisateur`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fiche_frais`
--

LOCK TABLES `fiche_frais` WRITE;
/*!40000 ALTER TABLE `fiche_frais` DISABLE KEYS */;
INSERT INTO `fiche_frais` VALUES (1,2,'2016-01-17 00:00:00',NULL,1),(2,1,'2016-01-11 00:00:00',NULL,1);
/*!40000 ALTER TABLE `fiche_frais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frais_forfait`
--

DROP TABLE IF EXISTS `frais_forfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frais_forfait` (
  `id_frais_forfait` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `montant` decimal(10,0) NOT NULL,
  `date_creation` date NOT NULL,
  `date_modif` date NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_frais_forfait`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frais_forfait`
--

LOCK TABLES `frais_forfait` WRITE;
/*!40000 ALTER TABLE `frais_forfait` DISABLE KEYS */;
INSERT INTO `frais_forfait` VALUES (1,'Forfait Etape',110,'2016-01-17','2016-01-17',''),(2,'Nuitée Hôtel',80,'2016-01-17','2016-01-17',''),(3,'Repas Restaurant',25,'2016-01-17','2016-01-17','');
/*!40000 ALTER TABLE `frais_forfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ligne_forfait`
--

DROP TABLE IF EXISTS `ligne_forfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ligne_forfait` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_frais_forfait` int(11) DEFAULT NULL,
  `fiche_frais` int(11) DEFAULT NULL,
  `etat` int(11) DEFAULT NULL,
  `date_creation` datetime NOT NULL,
  `date_modif` datetime DEFAULT NULL,
  `motif_refus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_242CFABB55CAF762` (`etat`),
  KEY `IDX_242CFABB5FC0A6A7` (`fiche_frais`),
  KEY `id_frais_forfait` (`id_frais_forfait`),
  CONSTRAINT `FK_242CFABB55CAF762` FOREIGN KEY (`etat`) REFERENCES `etat_ligne` (`id`),
  CONSTRAINT `FK_242CFABB5FC0A6A7` FOREIGN KEY (`fiche_frais`) REFERENCES `fiche_frais` (`id`),
  CONSTRAINT `FK_242CFABB862EECB` FOREIGN KEY (`id_frais_forfait`) REFERENCES `frais_forfait` (`id_frais_forfait`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ligne_forfait`
--

LOCK TABLES `ligne_forfait` WRITE;
/*!40000 ALTER TABLE `ligne_forfait` DISABLE KEYS */;
INSERT INTO `ligne_forfait` VALUES (1,1,1,NULL,'2016-01-17 00:00:00',NULL,NULL,1);
/*!40000 ALTER TABLE `ligne_forfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ligne_hors_forfait`
--

DROP TABLE IF EXISTS `ligne_hors_forfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ligne_hors_forfait` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fiche_frais` int(11) DEFAULT NULL,
  `etat` int(11) DEFAULT NULL,
  `justificatif_id` int(11) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `libelle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motif_refus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_creation` datetime NOT NULL,
  `date_modif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BF34AE8B4B85A991` (`justificatif_id`),
  KEY `IDX_BF34AE8B5FC0A6A7` (`fiche_frais`),
  KEY `IDX_BF34AE8B55CAF762` (`etat`),
  CONSTRAINT `FK_BF34AE8B4B85A991` FOREIGN KEY (`justificatif_id`) REFERENCES `document` (`id`),
  CONSTRAINT `FK_BF34AE8B55CAF762` FOREIGN KEY (`etat`) REFERENCES `etat_ligne` (`id`),
  CONSTRAINT `FK_BF34AE8B5FC0A6A7` FOREIGN KEY (`fiche_frais`) REFERENCES `fiche_frais` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ligne_hors_forfait`
--

LOCK TABLES `ligne_hors_forfait` WRITE;
/*!40000 ALTER TABLE `ligne_hors_forfait` DISABLE KEYS */;
INSERT INTO `ligne_hors_forfait` VALUES (1,1,NULL,NULL,14.34,'TEST',NULL,'2016-01-17 00:00:00',NULL),(2,2,NULL,NULL,42.11,'Admin',NULL,'2016-01-11 00:00:00',NULL);
/*!40000 ALTER TABLE `ligne_hors_forfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `repas_midi` double NOT NULL,
  `nuitee` double NOT NULL,
  `etape` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

-- Dump completed on 2016-02-11 16:21:07
