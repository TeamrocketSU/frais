<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use fraisBundle\Entity\Client;
use fraisBundle\Entity\FicheFrais;
use fraisBundle\Entity\FraisForfait;
use fraisBundle\Entity\LigneForfait;
use fraisBundle\Entity\LigneHorsForfait;
use fraisBundle\Entity\Utilisateur;
use fraisBundle\Ref\EtatFiche;
use fraisBundle\Ref\EtatLigne;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadDatas implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private static $regions = ['Paris-Centre', 'Sud', 'Nord', 'Ouest', 'Est', 'DTOM Caraïbes-Amériques', 'DTOM Asie-Afrique'];

    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }


    public function load(ObjectManager $manager)
    {

        $client = new Client;
        $client->setRandomId('3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4');
        $client->setRedirectUris([]);
        $client->setSecret('4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k');
        $client->setAllowedGrantTypes(['password']);
        $manager->persist($client);
        $manager->flush();


        $users = $this->loadUser($manager);
        $forfaits = $this->loadForfait($manager);
        $this->loadFicheFrais($manager, $users, $forfaits);

    }

    public function loadUser(ObjectManager $manager)
    {
        $users = [];
        $faker = $this->faker;

        $accountQuantity = [
            Utilisateur::ROLE_ADMINISTRATEUR => 3,
            Utilisateur::ROLE_COMPTABLE => 5,
            Utilisateur::ROLE_UTILISATEUR => 9
        ];

        foreach ($accountQuantity as $role => $number) {
            for ($i = 0; $i < $number; $i++) {

                $user = (new Utilisateur())
                    ->setDateEmbauche($faker->dateTimeBetween('-5 years', 'now'))
                    ->setVille($faker->city)
                    ->setCp($faker->postcode)
                    ->setEnabled(true)
                    ->setNom($faker->firstName)
                    ->setPrenom($faker->lastName)
                    ->setEmail($faker->email)
                    ->setUsername($faker->userName)
                    ->setPlainPassword('aa')
                    ->setAdresse($faker->streetAddress)
                    ->setRoles([$role]);

                if ($role === Utilisateur::ROLE_COMPTABLE) {
                    $user->setNumber($faker->phoneNumber);
                }
                $manager->persist($user);
                $users [] = $user;
            }
        }
        $manager->flush();

        return $users;
    }

    public function loadForfait(ObjectManager $manager)
    {
        $forfaits = [];
        foreach (self::$regions as $region) {
            foreach ($this->getType() as $type => $montant) {
                $forfait = (new FraisForfait())
                    ->setLibelle($type)
                    ->setMontant($montant)
                    ->setRegion($region);

                $manager->persist($forfait);
                $forfaits [] = $forfait;
            }
        }

        foreach ($this->getTypeKilometrique() as $type => $montant) {
            $forfait = (new FraisForfait())
                ->setRegion('Kilomètrique')
                ->setLibelle($type)
                ->setMontant($montant);

            $manager->persist($forfait);
            $forfaits [] = $forfait;
        }

        $manager->flush();
        return $forfaits;
    }

    private function getType()
    {
        return [
            'Hotel' => random_int(65, 90),
            'Repas' => random_int(25, 35),
            'Transport en commun' => random_int(5, 13),
        ];
    }

    private function getTypeKilometrique()
    {
        return [
            '4CV Diesel' => 0.52,
            '5/6CV Diesel' => 0.58,
            '4CV Essence' => 0.62,
            '5/6CV Essence' => 0.67
        ];
    }

    private function loadFicheFrais(ObjectManager $manager, array $users, array $forfaits)
    {
        /** @var Utilisateur $user */
        foreach ($users as $user) {
            if ($user->hasRole(Utilisateur::ROLE_UTILISATEUR)) {
                for ($i = mt_rand(-9, -1); $i <= 0; $i++) {
                    $date = new \DateTime('first day of ' . $i . ' month');

                    if ($i === 0) {
                        $etat = EtatFiche::CREATE;
                    } elseif ($i === -1) {
                        $etat = EtatFiche::CLOSE;
                    } else {
                        $etat = EtatFiche::REFUNDED;
                    }

                    $actual = $etat === EtatFiche::CREATE;

                    $fiche = new FicheFrais($user);
                    $fiche
                        ->setEtat($etat)
                        ->setCommentaire($this->faker->text(500))
                        ->setDateAction($date);

                    for ($j = mt_rand(0, 7); $j > 0; $j--) {
                        if (mt_rand(0, 2) === 1) {
                            $hf = $this->createHFLine($actual);
                            $manager->persist($hf);
                            $fiche->addLesLignesHorsForfait($hf);
                        } else {
                            $f = $this->createFLine($actual, $forfaits);
                            $manager->persist($f);
                            $fiche->addLesLignesForfait($f);
                        }
                    }
                    $manager->persist($fiche);
                }
            }
        }
        $manager->flush();
    }

    private function createHFLine($actual)
    {
        $hf = new LigneHorsForfait();
        $hf
            ->setMontant($this->faker->randomFloat(2, 3, 50))
            ->setLibelle($this->faker->text(30))
            ->setDateCreation($this->faker->dateTimeBetween('-30 days', 'now'));

        if ($actual !== true) {
            $hf->setEtat((mt_rand(0, 1) === 1) ? EtatLigne::VALID : EtatLigne::REFUSE);
        }
        return $hf;
    }

    private function createFLine($actual, array $forfaits)
    {
        $f = new LigneForfait();
        $f
            ->setDateCreation($this->faker->dateTimeBetween('-30 days', 'now'))
            ->setQuantite(mt_rand(1, 8))
            ->setIdFraisForfait($this->faker->randomElement($forfaits));

        if ($actual !== true) {
            $f->setEtat((mt_rand(0, 1) === 1) ? EtatLigne::VALID : EtatLigne::REFUSE);
        }
        return $f;
    }
}