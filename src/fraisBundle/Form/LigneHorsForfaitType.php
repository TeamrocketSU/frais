<?php

namespace fraisBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LigneHorsForfaitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateCreation', 'date', array(
                'label' => false,
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'data-uk-datepicker' => '{format:\'DD/MM/YYYY\'}',
                    'placeholder' => '00/00/0000'
                ),
            ))
            ->add('libelle', 'text', array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Saisir un libellé'
                ),
            ))
            ->add('montant', MoneyType::class, array(
                'label' => false,
                'currency' => false,
                'attr' => array(
                    'placeholder' => 'Montant'
                ),
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'fraisBundle\Entity\LigneHorsForfait'
        ));
    }
}
