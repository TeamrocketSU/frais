<?php

namespace fraisBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LigneForfaitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateCreation', DateType::class, array(
                'label' => false,
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'data-uk-datepicker' => '{format:\'DD/MM/YYYY\'}',
                    'placeholder' => '00/00/0000'
                ),
            ))
            ->add('idFraisForfait', EntityType::class, array(
                'label' => false,
                'class' => 'fraisBundle\Entity\FraisForfait',
                'group_by' => 'region'
            ))
            ->add('quantite', IntegerType::class, array(
                'label' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'fraisBundle\Entity\LigneForfait'
        ));
    }
}
