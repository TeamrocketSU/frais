<?php

namespace fraisBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class fraisBundle extends Bundle
{

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
