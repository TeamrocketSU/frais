<?php
/**
 * Created by PhpStorm.
 * User: Allan
 * Date: 03/04/2016
 * Time: 09:53
 */

namespace fraisBundle\Ref;


class EtatLigne
{
    const CREATE = "Créée";
    const REFUSE = "Refusée";
    const VALID = "Validée";
    const WAIT_DOCUMENTS = "En attente de documents";

    public static function getList()
    {
        return [
            self::CREATE,
            self::REFUSE,
            self::VALID,
            self::WAIT_DOCUMENTS,
        ];
    }
}