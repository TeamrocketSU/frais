<?php
/**
 * Created by PhpStorm.
 * User: Allan
 * Date: 03/04/2016
 * Time: 09:53
 */

namespace fraisBundle\Ref;


class EtatFiche
{
    const CREATE = "Créée";
    const CLOSE = "Cloturée";
    const VALID = "Validée";
    const ON_PAYMENT = "Mise en paiement";
    const REFUNDED = "Remboursée";

    public static function getList()
    {
        return [
            self::CREATE,
            self::CLOSE,
            self::VALID,
            self::ON_PAYMENT,
            self::REFUNDED,
        ];
    }
}