<?php

namespace fraisBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;

/**
 * @ORM\Entity(repositoryClass="fraisBundle\Repository\UtilisateurRepository")
 * @ORM\Table(name="utilisateur")
 * @ORM\HasLifecycleCallbacks()
 */
class Utilisateur extends BaseUser
{
    const ROLE_UTILISATEUR = "ROLE_UTILISATEUR";
    const ROLE_COMPTABLE = "ROLE_COMPTABLE";
    const ROLE_ADMINISTRATEUR = "ROLE_ADMINISTRATEUR";
    public $type;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=30, nullable=false)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=5, nullable=false)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=30, nullable=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=10, nullable=true)
     */
    private $number;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_embauche", type="date", nullable=false)
     */
    private $dateEmbauche;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="fraisBundle\Entity\FicheFrais", mappedBy="utilisateur")
     */
    private $lesFiches;

    public static function getRolesAvailable()
    {
        return array(
            self::ROLE_UTILISATEUR => 'Utilisateur',
            self::ROLE_ADMINISTRATEUR => 'Administrateur',
            self::ROLE_COMPTABLE => 'Comptable'
        );
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return $this
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return $this
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return $this
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return $this
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return $this
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get dateEmbauche
     *
     * @return \DateTime
     */
    public function getDateEmbauche()
    {
        return $this->dateEmbauche;
    }

    /**
     * Set dateEmbauche
     *
     * @param \DateTime $dateEmbauche
     *
     * @return $this
     */
    public function setDateEmbauche($dateEmbauche)
    {
        $this->dateEmbauche = $dateEmbauche;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\PreFlush
     */
    public function defineRole()
    {
        if ($this->type != null) {
            $this->setRoles(array($this->type));
        }

    }

    /**
     * Add lesFich
     *
     * @param FicheFrais $lesFich
     *
     * @return $this
     */
    public function addLesFich(FicheFrais $lesFich)
    {
        $this->lesFiches[] = $lesFich;

        return $this;
    }

    /**
     * Remove lesFich
     *
     * @param FicheFrais $lesFich
     */
    public function removeLesFich(FicheFrais $lesFich)
    {
        $this->lesFiches->removeElement($lesFich);
    }

    /**
     * Get lesFiches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLesFiches()
    {
        return $this->lesFiches;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Tells if the the given user is this user.
     *
     * Useful when not hydrating all fields.
     *
     * @param null|UserInterface $user
     *
     * @return boolean
     */
    public function isUser(UserInterface $user = null)
    {
        return $user === $this;
    }
}
