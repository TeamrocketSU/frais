<?php

namespace fraisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use fraisBundle\Ref\EtatLigne;

/**
 * LigneForfait
 *
 * @ORM\Table(name="ligne_forfait", indexes={@ORM\Index(name="IDX_242CFABB55CAF762", columns={"etat"}), @ORM\Index(name="IDX_242CFABB5FC0A6A7", columns={"fiche_frais"}), @ORM\Index(name="id_frais_forfait", columns={"id_frais_forfait"})})
 * @ORM\Entity(repositoryClass="fraisBundle\Repository\LigneForfaitRepository")
 */
class LigneForfait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modif", type="datetime", nullable=true)
     */
    private $dateModif;


    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer", nullable=false)
     */
    private $quantite = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var FraisForfait
     *
     * @ORM\ManyToOne(targetEntity="fraisBundle\Entity\FraisForfait")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_frais_forfait", referencedColumnName="id_frais_forfait")
     * })
     */
    private $idFraisForfait;

    /**
     * @var FicheFrais
     *
     * @ORM\ManyToOne(targetEntity="fraisBundle\Entity\FicheFrais", inversedBy="lesLignesForfaits")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fiche_frais", referencedColumnName="id")
     * })
     */
    private $ficheFrais;

    /**
     * @var String
     * @ORM\Column(name="etat", type="string", length=35)
     *
     */
    private $etat;


    public function __construct()
    {
        $this->etat = EtatLigne::CREATE;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return LigneForfait
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return LigneForfait
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }


    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return LigneForfait
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get idFraisForfait
     *
     * @return FraisForfait
     */
    public function getIdFraisForfait()
    {
        return $this->idFraisForfait;
    }

    /**
     * Set idFraisForfait
     *
     * @param FraisForfait $idFraisForfait
     *
     * @return LigneForfait
     */
    public function setIdFraisForfait(FraisForfait $idFraisForfait = null)
    {
        $this->idFraisForfait = $idFraisForfait;

        return $this;
    }

    /**
     * Get ficheFrais
     *
     * @return FicheFrais
     */
    public function getFicheFrais()
    {
        return $this->ficheFrais;
    }

    /**
     * Set ficheFrais
     *
     * @param FicheFrais $ficheFrais
     *
     * @return LigneForfait
     */
    public function setFicheFrais(FicheFrais $ficheFrais = null)
    {
        $this->ficheFrais = $ficheFrais;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return LigneForfait
     * @throws \LogicException
     */
    public function setEtat($etat = null)
    {
        if (!in_array($etat, EtatLigne::getList())) {
            throw new \LogicException('This State is not valid');
        }

        $this->etat = $etat;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->dateModif = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->dateCreation = new \DateTime();
    }

    public function __toString()
    {
        return $this->idFraisForfait->getLibelle();
    }

    public function getMontant()
    {
        return $this->quantite * $this->idFraisForfait->getMontant();
    }

    public function isValid()
    {
        return ($this->etat === EtatLigne::VALID);
    }

    public function getDescription()
    {
        return $this->idFraisForfait->getRegion() . ' > ' . $this->idFraisForfait->getLibelle() . ' (Qtt:' . $this->quantite . ')';
    }
}
