<?php

namespace fraisBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use fraisBundle\Ref\EtatFiche;
use fraisBundle\Ref\EtatLigne;
use IntlDateFormatter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FicheFrais
 *
 * @ORM\Table(name="fiche_frais",
 *      indexes={
 *          @ORM\Index(name="idx_annee_mois", columns={"date_action"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="fraisBundle\Repository\FicheFraisRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FicheFrais
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_action", type="date", nullable=false)
     */
    private $dateAction;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="date", nullable=false)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime", nullable=true)
     */
    private $dateModification;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="fraisBundle\Entity\Utilisateur", inversedBy="lesFiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @var String EtatFiche
     *
     * @ORM\Column(name="etat", type="text", length=35)
     */
    private $etat;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="fraisBundle\Entity\Document", mappedBy="fiche", cascade={"persist"})
     */
    private $documents;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="fraisBundle\Entity\LigneForfait", mappedBy="ficheFrais", cascade={"persist"})
     */
    private $lesLignesForfaits;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="fraisBundle\Entity\LigneHorsForfait", mappedBy="ficheFrais", cascade={"persist"})
     */
    private $lesLignesHorsForfaits;

    /**
     * Constructor
     * @param UserInterface $user
     */

    /**
     * @var String Commentaire
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    public function __construct(UserInterface $user)
    {
        $this->lesLignesForfaits = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->lesLignesHorsForfaits = new ArrayCollection();
        $this->dateCreation = new \DateTime();

        $this->utilisateur = $user;
        $this->setEtat(EtatFiche::CREATE);
    }

    /**
     * Get dateModification
     *
     * @return \DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get utilisateur
     *
     * @return Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Get etat
     *
     * @return EtatFiche
     */
    public function getEtat()
    {
        return $this->etat;
    }


    /**
     * Set etat
     *
     * @param String $etat
     *
     * @return $this
     */
    public function setEtat($etat)
    {
        if (!in_array($etat, EtatFiche::getList())) {
            throw new \LogicException('This State is not valid');
        }

        $this->etat = $etat;

        return $this;
    }

    /**
     * Add lesLignesForfait
     *
     * @param LigneForfait $lesLignesForfait
     *
     * @return FicheFrais
     */
    public function addLesLignesForfait(LigneForfait $lesLignesForfait)
    {
        $lesLignesForfait->setFicheFrais($this);
        $this->lesLignesForfaits[] = $lesLignesForfait;

        return $this;
    }

    /**
     * Remove lesLignesForfait
     *
     * @param LigneForfait $lesLignesForfait
     */
    public function removeLesLignesForfait(LigneForfait $lesLignesForfait)
    {
        $this->lesLignesForfaits->removeElement($lesLignesForfait);
    }

    /**
     * Get lesLignesForfaits
     *
     * @return LigneForfait[]|ArrayCollection
     */
    public function getLesLignesForfaits()
    {
        return $this->lesLignesForfaits;
    }

    /**
     * Add lesLignesHorsForfait
     *
     * @param LigneHorsForfait $lesLignesHorsForfait
     *
     * @return FicheFrais
     */
    public function addLesLignesHorsForfait(LigneHorsForfait $lesLignesHorsForfait)
    {
        $lesLignesHorsForfait->setFicheFrais($this);
        $this->lesLignesHorsForfaits[] = $lesLignesHorsForfait;

        return $this;
    }

    /**
     * Remove lesLignesHorsForfait
     *
     * @param LigneHorsForfait $lesLignesHorsForfait
     */
    public function removeLesLignesHorsForfait(LigneHorsForfait $lesLignesHorsForfait)
    {
        $this->lesLignesHorsForfaits->removeElement($lesLignesHorsForfait);
    }

    /**
     * Get lesLignesHorsForfaits
     *
     * @return LigneHorsForfait[]|ArrayCollection
     */
    public function getLesLignesHorsForfaits()
    {
        return $this->lesLignesHorsForfaits;
    }


    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->dateModification = new \DateTime();
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $fmt = new IntlDateFormatter("fr_FR", IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE,
            'Europe/Paris', IntlDateFormatter::GREGORIAN, "MMMM y");

        return "Fiche du mois de " . $fmt->format($this->dateAction);
    }


    /**
     * Add document
     *
     * @param Document $document
     *
     * @return FicheFrais
     */
    public function addDocument(Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @return \DateTime
     */
    public function getDateAction()
    {
        return $this->dateAction;
    }

    /**
     * @param \DateTime $dateAction
     * @return $this
     */
    public function setDateAction(\DateTime $dateAction)
    {
        $dateAction->modify('first day of this month');
        $this->dateAction = $dateAction;

        return $this;
    }

    public function getMontant()
    {
        return $this->getMontantForfait() + $this->getMontantHorsForfait();
    }

    public function getMontantForfait()
    {
        $total = 0;
        /** @var LigneForfait $ligne */
        foreach ($this->lesLignesForfaits as $ligne) {
            $total += $ligne->getMontant();
        }
        return $total;
    }

    public function getMontantHorsForfait()
    {
        $total = 0;

        /** @var LigneHorsForfait $ligne */
        foreach ($this->lesLignesHorsForfaits as $ligne) {
            $total += $ligne->getMontant();
        }
        return $total;
    }

    public function getMontantValide()
    {
        return $this->getMontantForfaitValide() + $this->getMontantHorsForfaitValide();
    }

    public function getMontantForfaitValide()
    {
        $total = 0;

        /** @var LigneHorsForfait $ligne */
        foreach ($this->lesLignesForfaits as $ligne) {
            if ($ligne->isValid() === true) {
                $total += $ligne->getMontant();
            }
        }
        return $total;
    }

    public function getMontantHorsForfaitValide()
    {
        $total = 0;

        /** @var LigneHorsForfait $ligne */
        foreach ($this->lesLignesHorsForfaits as $ligne) {
            if ($ligne->isValid() === true) {
                $total += $ligne->getMontant();
            }
        }
        return $total;
    }

    public function checkState()
    {
        $isValid = true;
        /** @var LigneForfait $line */
        foreach (array_merge($this->lesLignesForfaits->toArray(), $this->lesLignesHorsForfaits->toArray()) as $line) {
            if ($line->getEtat() == EtatLigne::CREATE) {
                $isValid = false;
                break;
            }
        }

        if (true === $isValid) {
            $this->setEtat(EtatFiche::VALID);
        }
    }
}
