<?php

namespace fraisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="fraisBundle\Repository\DocumentRepository")
 * @Vich\Uploadable
 */
class Document
{
    /**
     * @var File
     *
     * @Assert\NotBlank()
     * @Assert\File()
     * @Vich\UploadableField(mapping="justificatifs", fileNameProperty="nom")
     */
    private $fichier;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime", nullable=true)
     */
    private $date_modification;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var FicheFrais
     * @ORM\ManyToOne(targetEntity="fraisBundle\Entity\FicheFrais", inversedBy="documents")
     */
    private $fiche;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->date_modification = new \DateTime();
    }


    /**
     * @return File
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichier
     *
     * @return File
     */
    public function setFichier(File $fichier = null)
    {
        $this->fichier = $fichier;

        if ($fichier) {
            $this->date_modification = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Document
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime
     */
    public function getDateModification()
    {
        return $this->date_modification;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     *
     * @return Document
     */
    public function setDateModification($dateModification)
    {
        $this->date_modification = $dateModification;

        return $this;
    }

    /**
     * Get fiche
     *
     * @return FicheFrais
     */
    public function getFiche()
    {
        return $this->fiche;
    }

    /**
     * Set fiche
     *
     * @param FicheFrais $fiche
     *
     * @return Document
     */
    public function setFiche(FicheFrais $fiche = null)
    {
        $this->fiche = $fiche;

        return $this;
    }

}
