<?php

namespace fraisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use fraisBundle\Ref\EtatLigne;

/**
 * LigneHorsForfait
 *
 * @ORM\Table(name="ligne_hors_forfait", indexes={@ORM\Index(name="IDX_BF34AE8B5FC0A6A7", columns={"fiche_frais"}), @ORM\Index(name="IDX_BF34AE8B55CAF762", columns={"etat"})})
 * @ORM\Entity(repositoryClass="fraisBundle\Repository\LigneHorsForfaitRepository")
 */
class LigneHorsForfait
{
    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", precision=10, scale=0, nullable=true)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=100, nullable=true)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modif", type="datetime", nullable=true)
     */
    private $dateModif;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var FicheFrais
     *
     * @ORM\ManyToOne(targetEntity="fraisBundle\Entity\FicheFrais", inversedBy="lesLignesHorsForfaits")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fiche_frais", referencedColumnName="id")
     * })
     */
    private $ficheFrais;


    /**
     * @var String
     * @ORM\Column(name="etat", type="string", length=35)
     *
     */
    private $etat;

    /**
     * LigneHorsForfait constructor.
     */
    public function __construct()
    {
        $this->etat = EtatLigne::CREATE;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return LigneHorsForfait
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return LigneHorsForfait
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return LigneHorsForfait
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return LigneHorsForfait
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get ficheFrais
     *
     * @return FicheFrais
     */
    public function getFicheFrais()
    {
        return $this->ficheFrais;
    }

    /**
     * Set ficheFrais
     *
     * @param FicheFrais $ficheFrais
     *
     * @return LigneHorsForfait
     */
    public function setFicheFrais(FicheFrais $ficheFrais = null)
    {
        $this->ficheFrais = $ficheFrais;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set etat
     *
     * @param string
     *
     * @return LigneHorsForfait
     */
    public function setEtat($etat = null)
    {
        if (!in_array($etat, EtatLigne::getList())) {
            throw new \LogicException('This State is not valid');
        }

        $this->etat = $etat;

        return $this;
    }

    public function __toString()
    {
        return $this->libelle;
    }

    public function isValid()
    {
        return ($this->etat === EtatLigne::VALID);
    }
}
