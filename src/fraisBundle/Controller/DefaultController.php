<?php

namespace fraisBundle\Controller;

use fraisBundle\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        if ($this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            return $this->redirectToRoute('frais_Utilisateur_list');
        } elseif ($this->isGranted(Utilisateur::ROLE_UTILISATEUR)) {
            return $this->redirectToRoute('frais_visiteur_accueil');
        } elseif ($this->isGranted(Utilisateur::ROLE_COMPTABLE)) {
            return $this->redirectToRoute('frais_compta_accueil');
        }

        throw $this->createAccessDeniedException();
    }
}
