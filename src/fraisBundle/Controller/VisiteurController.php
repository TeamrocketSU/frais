<?php

namespace fraisBundle\Controller;

use fraisBundle\Entity\Document;
use fraisBundle\Entity\FicheFrais;
use fraisBundle\Entity\LigneForfait;
use fraisBundle\Entity\LigneHorsForfait;
use fraisBundle\Entity\Utilisateur;
use fraisBundle\Form\FicheFraisType;
use fraisBundle\Form\LigneForfaitType;
use fraisBundle\Form\LigneHorsForfaitType;
use fraisBundle\Form\DocumentType;
use fraisBundle\Ref\EtatFiche;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VisiteurController extends Controller
{
    public function indexAction(Request $request, $month = null, $year = null)
    {
        if (!$this->isGranted(Utilisateur::ROLE_UTILISATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $ficheFraisManager = $this->get('frais.fiche_frais_manager');

        if ($month !== null and $year !== null) {
            $currentFiche = $ficheFraisManager->getFicheFor((int)$year, (int)$month, $this->getUser());
            if ($currentFiche === null) {
                throw $this->createNotFoundException('fiche non existante');
            }
        } else {
            $currentFiche = $ficheFraisManager->getCurrentFiche($this->getUser());
        }
        $formParam = array();
        if ($currentFiche->getEtat() === EtatFiche::CREATE) {
            /********************
             ***     FORM     ***
             *******************/
            //Forfait
            $ligne = new LigneForfait();

            $ligneForfaitForm = $this->createForm(LigneForfaitType::class, $ligne);
            $ligneForfaitForm->add('submit', SubmitType::class, array(
                'label' => ' '
            ));
            $ligneForfaitForm->handleRequest($request);

            if ($ligneForfaitForm->isValid()) {
                $ligne->setFicheFrais($currentFiche);
                $currentFiche->addLesLignesForfait($ligne);
                $ficheFraisManager->save($currentFiche);

                $this->addFlash('success', 'Ligne ajoutée');
                return $this->redirectToRoute("frais_visiteur_accueil");
            }

            //Hors forfait

            $ligne = new LigneHorsForfait();

            $ligneHorsForfaitForm = $this->createForm(LigneHorsForfaitType::class, $ligne);
            $ligneHorsForfaitForm->add('submit', SubmitType::class, array(
                'label' => ' '
            ));
            $ligneHorsForfaitForm->handleRequest($request);

            if ($ligneHorsForfaitForm->isValid()) {
                $ligne->setFicheFrais($currentFiche);
                $currentFiche->addLesLignesHorsForfait($ligne);

                $ficheFraisManager->save($currentFiche);

                $this->addFlash('success', 'Ligne ajoutée');
                return $this->redirectToRoute("frais_visiteur_accueil");
            }

            //Documents

            $document = new Document();

            $documentForm = $this->createForm(DocumentType::class, $document);
            $documentForm->add('submit', SubmitType::class, array(
                'label' => ' '
            ));
            $documentForm->handleRequest($request);

            if ($documentForm->isValid()) {
                $document->setFiche($currentFiche);
                $currentFiche->addDocument($document);

                $ficheFraisManager->save($currentFiche);

                $this->addFlash('success', 'Document ajouté');
                return $this->redirectToRoute("frais_visiteur_accueil");
            }

            //Forfait
            $commentaireForm = $this->createForm(FicheFraisType::class, $currentFiche);
            $commentaireForm->add('submit', SubmitType::class, array(
                'label' => ' '
            ));
            $commentaireForm->handleRequest($request);

            if ($commentaireForm->isValid()) {
                $ficheFraisManager->save($currentFiche);

                $this->addFlash('success', 'Commentaire ajouté');
                return $this->redirectToRoute("frais_visiteur_accueil");
            }

            $formParam = array(
                'formLigneForfait' => $ligneForfaitForm->createView(),
                'formLigneHorsForfait' => $ligneHorsForfaitForm->createView(),
                'formDocument' => $documentForm->createView(),
                'formCommentaire' => $commentaireForm->createView(),
            );

        }

        $params = array(
            'listFiche' => $ficheFraisManager->getListFicheOfYear($this->getUser()),
            'currentFiche' => $currentFiche
        );


        return $this->render('fraisBundle:Visiteur:index.html.twig',
            array_merge($params, $formParam)
        );
    }

    public function deleteLigneForfaitAction(LigneForfait $ligneForfait)
    {
        if (!$this->isGranted(Utilisateur::ROLE_UTILISATEUR)) {
            throw $this->createAccessDeniedException();
        }
        $fiche = $ligneForfait->getFicheFrais();
        $fiche->removeLesLignesForfait($ligneForfait);
        $em = $this->getDoctrine()->getManager();

        $em->remove($ligneForfait);
        $em->flush();

        $this->addFlash("info", "La ligne forfait n° " .$ligneForfait->getId(). " a été supprimée");

        return $this->redirectToRoute("frais_visiteur_accueil");
    }

    public function deleteLigneHorsForfaitAction(LigneHorsForfait $ligneHorsForfait)
    {
        if (!$this->isGranted(Utilisateur::ROLE_UTILISATEUR)) {
            throw $this->createAccessDeniedException();
        }
        $fiche = $ligneHorsForfait->getFicheFrais();
        $fiche->removeLesLignesHorsForfait($ligneHorsForfait);
        $em = $this->getDoctrine()->getManager();

        $em->remove($ligneHorsForfait);
        $em->flush();

        $this->addFlash("info", "La ligne hors forfait n° " .$ligneHorsForfait->getId(). " a été supprimée");

        return $this->redirectToRoute("frais_visiteur_accueil");
    }

    public function editLigneForfaitAction(Request $request, LigneForfait $ligneForfait)
    {
        $deleteForm = $this->createForm(new LigneForfaitType(), $ligneForfait);
        $editForm = $this->createForm('fraisBundle\Form\LigneForfaitType', $ligneForfait);
        $editForm
            ->add('save', SubmitType::class, array('label' => 'Mettre à jour'));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ligneForfait);
            $em->flush();

            return $this->redirectToRoute('frais_visiteur_accueil');
        }

        return $this->render('fraisBundle:Visiteur:edit.html.twig', array(
            'article' => $ligneForfait,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editLigneHorsForfaitAction(Request $request, LigneHorsForfait $ligneHorsForfait)
    {
        $deleteForm = $this->createForm(new LigneHorsForfaitType(), $ligneHorsForfait);
        $editForm = $this->createForm('fraisBundle\Form\LigneHorsForfaitType', $ligneHorsForfait);
        $editForm
            ->add('save', SubmitType::class, array('label' => 'Mettre à jour'));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ligneHorsForfait);
            $em->flush();

            return $this->redirectToRoute('frais_visiteur_accueil');
        }

        return $this->render('fraisBundle:Visiteur:edit.html.twig', array(
            'article' => $ligneHorsForfait,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteDocumentAction(Document $document)
    {
        if (!$this->isGranted(Utilisateur::ROLE_UTILISATEUR)) {
            throw $this->createAccessDeniedException();
        }
        $fiche = $document->getFiche();
        $fiche->removeDocument($document);
        $em = $this->getDoctrine()->getManager();

        $em->remove($document);
        $em->flush();

        $this->addFlash("info", "Le document a été supprimé");

        return $this->redirectToRoute("frais_visiteur_accueil");
    }

    public function toPdfAction($month, $year)
    {
        if (!$this->isGranted(Utilisateur::ROLE_UTILISATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $ficheFraisManager = $this->get('frais.fiche_frais_manager');

        if ($month !== null and $year !== null) {
            $currentFiche = $ficheFraisManager->getFicheFor((int)$year, (int)$month, $this->getUser());
            if ($currentFiche === null) {
                throw $this->createNotFoundException('fiche non existante');
            }
        } else {
            $currentFiche = $ficheFraisManager->getCurrentFiche($this->getUser());
        }

        $params = array(
            'listFiche' => $ficheFraisManager->getListFicheOfYear($this->getUser()),
            'currentFiche' => $currentFiche,
        );

        $html = $this->renderView('fraisBundle:Visiteur:view.html.twig',
            array_merge($params)
        );

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="fiche_'.$year.'_'.$month.'.pdf"'
            )
        );

    }
}