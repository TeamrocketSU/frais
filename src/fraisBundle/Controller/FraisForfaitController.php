<?php

namespace fraisBundle\Controller;

use Doctrine\ORM\EntityManager;
use fraisBundle\Entity\FraisForfait;
use fraisBundle\Entity\Utilisateur;
use fraisBundle\Form\FraisForfaitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FraisForfaitController extends Controller
{

    /**
     * @return Response
     * @throws \Exception
     */
    public function listAction()
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }
        $repo = $this->getDoctrine()->getRepository('fraisBundle:FraisForfait');


        return $this->render($this->getTemplateNamespace() . 'list.html.twig', array(
            "list" => $repo->findAll(),
        ));
    }

    /**
     * Return string of location of template like : AppBundle:MY_ENTITY_NAME
     * @return string
     */
    protected function getTemplateNamespace()
    {
        return 'fraisBundle:FraisForfait:';
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $entity = new FraisForfait();

        $form = $this->createForm(new FraisForfaitType(), $entity);

        $form
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->add('back', ButtonType::class, array('label' => 'Retour'));

        $form->handleRequest($request);
        if ($form->isValid()) {

            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();

            $this->addFlash("info", "Le forfait : " . $entity . " a été créé");
            return $this->redirectToList();
        }

        return $this->render($this->getTemplateNamespace() . 'create.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'listRoute' => $this->getListRoute(),
            'templateNamespace' => $this->getTemplateNamespace(),

        ));
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return RedirectResponse
     */
    protected function redirectToList()
    {
        return $this->redirectToRoute($this->getListRoute());
    }

    /**
     * @return string
     */
    protected function getListRoute()
    {
        return 'frais_FraisForfait_list';

    }

    /**
     * @param Request $request
     * @param FraisForfait $entity
     * @return Response
     */
    public function updateAction(Request $request, FraisForfait $entity)
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(new FraisForfaitType(), $entity);

        $form
            ->add('save', SubmitType::class, array('label' => 'Mettre à jour'))
            ->add('back', ButtonType::class, array('label' => 'Retour'));

        $form->handleRequest($request);
        if ($form->isValid()) {


            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();

            $this->addFlash("info", "L'utilisateur : " . $entity . " a été mis a jour");
            return $this->redirectToList();
        }

        return $this->render($this->getTemplateNamespace() . 'update.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'listRoute' => $this->getListRoute(),
            'templateNamespace' => $this->getTemplateNamespace(),
        ));
    }

    /**
     * @param FraisForfait $entity
     * @return RedirectResponse
     */
    public function deleteAction(FraisForfait $entity)
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();

        $this->addFlash("info", "Le forfait : " . $entity . " a été supprimé");

        return $this->redirectToList();
    }

    /**
     * @param FraisForfait $entity
     * @return Response
     */
    public function viewAction(FraisForfait $entity)
    {
        return $this->render($this->getTemplateNamespace() . 'view.html.twig', array(
            'entity' => $entity
        ));
    }
}