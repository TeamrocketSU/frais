<?php

namespace fraisBundle\Controller;

use fraisBundle\Entity\FicheFrais;
use fraisBundle\Entity\Utilisateur;
use fraisBundle\Form\FindUtilisateurType;
use fraisBundle\Ref\EtatLigne;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ComptableController extends Controller
{
    const CAN_VALID = true;
    const CAN_NOT_VALID = false;

    public function indexAction()
    {
        $state = $this->checkState();

        $fiches = $this->get('frais.fiche_frais_manager')->getListFicheSeekValid();

        return $this->render('fraisBundle:Comptable:index.html.twig', array(
            'state' => $state,
            'fiches' => $fiches,
        ));
    }

    private function checkState()
    {
        if (!$this->isGranted(Utilisateur::ROLE_COMPTABLE)) {
            throw $this->createAccessDeniedException();
        }

        $dateDebutSaisieFrais = $this->getParameter('date_debut_validation_frais');
        $dateFinSaisieFrais = $this->getParameter('date_fin_validation_frais');
        //state
        $now = new \DateTime('now');
        $day = $now->format('j');
        return ($day >= $dateDebutSaisieFrais && $day <= $dateFinSaisieFrais) ? self::CAN_VALID : self::CAN_NOT_VALID;
    }

    public function manageLineAction($type, $id, $state)
    {
        $this->checkState();
        $manager = $this->get('frais.fiche_frais_manager');
        $em = $this->getDoctrine()->getManager();
        $line = null;

        if ("f" === $type) {
            $line = $em->getRepository('fraisBundle:LigneForfait')->find($id);
        } elseif ("hf" === $type) {
            $line = $em->getRepository('fraisBundle:LigneHorsForfait')->find($id);
        }

        if (null === $line) {
            throw $this->createNotFoundException();
        }

        if (false === in_array($state, EtatLigne::getList())) {
            throw new \LogicException('this state is not supported : ' . $state);
        }


        $baseFiche = $line->getFicheFrais();
        if ($type == "hf" and $state == EtatLigne::WAIT_DOCUMENTS) {
            $targetFiche = $manager->getCurrentFiche($line->getFicheFrais()->getUtilisateur());
            $line->setFicheFrais($targetFiche);
            $em->persist($targetFiche);
        }


        $line->setEtat($state);
        $baseFiche->checkState();
        $em->flush();

        $this->addFlash('success', 'L\'état a été changer.');

        return $this->redirectToRoute('frais_compta_view_fiche', array(
            'id' => $baseFiche->getId(),
        ));
    }

    public function showAction(FicheFrais $fiche)
    {
        $this->checkState();
        $manager = $this->get('frais.fiche_frais_manager');


        return $this->render('fraisBundle:Comptable:show.html.twig', array(
            'listFiche' => $manager->getListFicheOfYear($fiche->getUtilisateur()),
            'currentFiche' => $fiche,
        ));
    }


    public function historyAction(Request $request)
    {
        $manager = $this->get('frais.fiche_frais_manager');
        $form = $this->createForm(FindUtilisateurType::class);
        $form->add('ok', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $form->get('user')->getData();
            $last = $manager->getLastFiche($user);

            if ($last === null) {
                $this->addFlash('danger', 'L\'utilisateur n\'as de fiche.');
            } else {
                return $this->redirectToRoute('frais_compta_view_fiche', array('id' => $last->getId()));
            }
        }

        return $this->render('fraisBundle:Comptable:history.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
