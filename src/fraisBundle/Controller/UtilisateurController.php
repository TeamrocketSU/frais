<?php

namespace fraisBundle\Controller;

use Doctrine\ORM\EntityManager;
use fraisBundle\Entity\Utilisateur;
use fraisBundle\Form\UtilisateurType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UtilisateurController extends Controller
{

    /**
     * @return Response
     * @throws \Exception
     */
    public function listAction()
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }
        $repo = $this->getDoctrine()->getRepository('fraisBundle:Utilisateur');


        return $this->render($this->getTemplateNamespace() . 'list.html.twig', array(
            "list" => $repo->findAll(),
        ));
    }

    /**
     * Return string of location of template like : AppBundle:MY_ENTITY_NAME
     * @return string
     */
    protected function getTemplateNamespace()
    {
        return 'fraisBundle:Utilisateur:';
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $entity = new Utilisateur();

        $form = $this->createForm(new UtilisateurType(), $entity);

        $form
            ->add('save', SubmitType::class, array('label' => 'Ajouter'))
            ->add('back', ButtonType::class, array('label' => 'Retour'));

        $form->handleRequest($request);
        if ($form->isValid()) {

            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();

            $this->addFlash("info", "L'utilisateur : " . $entity . " a été créé");

            if ($request->isXmlHttpRequest()) {
                $html = $this
                    ->render('fraisBundle:Utilisateur:ligne_user_content.html.twig', array('user' => $entity))
                    ->getContent();

                $response = array(
                    'data' => $html,
                );

                return new JsonResponse($response, 200);

            } else {

                return $this->redirectToList();
            }


        }


        return $this->render($this->getTemplateNamespace() . 'create.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'listRoute' => $this->getListRoute(),
            'templateNamespace' => $this->getTemplateNamespace(),

        ));
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return RedirectResponse
     */
    protected function redirectToList()
    {
        return $this->redirectToRoute($this->getListRoute());
    }

    /**
     * @return string
     */
    protected function getListRoute()
    {
        return 'frais_Utilisateur_list';

    }

    /**
     * @param Request $request
     * @param Utilisateur $entity
     * @return Response
     */
    public function updateAction(Request $request, Utilisateur $entity)
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(new UtilisateurType(), $entity);

        $form
            ->add('save', SubmitType::class, array('label' => 'Mettre à jour'))
            ->add('back', ButtonType::class, array('label' => 'Retour'));

        $form->handleRequest($request);
        if ($form->isValid()) {


            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();

            $this->addFlash("info", "L'utilisateur : " . $entity . " a été mis a jour");
            return $this->redirectToList();
        }

        return $this->render($this->getTemplateNamespace() . 'update.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'listRoute' => $this->getListRoute(),
            'templateNamespace' => $this->getTemplateNamespace(),
        ));
    }

    /**
     * @param Utilisateur $entity
     * @return RedirectResponse
     */
    public function deleteAction(Utilisateur $entity)
    {
        if (!$this->isGranted(Utilisateur::ROLE_ADMINISTRATEUR)) {
            throw $this->createAccessDeniedException();
        }

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();

        $this->addFlash("info", "L'utilisateur : " . $entity . " a été supprimé");

        return $this->redirectToList();
    }

    /**
     * @param Utilisateur $entity
     * @return Response
     */
    public function viewAction(Utilisateur $entity)
    {
        return $this->render($this->getTemplateNamespace() . 'view.html.twig', array(
            'entity' => $entity
        ));
    }
}