<?php

namespace fraisBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;

class RestController extends FOSRestController
{
    public function getDemosAction()
    {
        $data = array("hello" => "world");
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function getFichesAction()
    {
        $context = SerializationContext::create()->setGroups(array('Default'));

        $data = $this->getDoctrine()->getRepository('fraisBundle:FicheFrais')->findListFicheOfYear($this->getUser());
        $view = $this->view($data);
        $view->setSerializationContext($context);
        return $this->handleView($view);
    }

    public function getContactableComptableAction()
    {
        $data = $this->getDoctrine()->getRepository('fraisBundle:Utilisateur')->getContactableComptable();
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function getFicheDetailAction($id)
    {
        $context = SerializationContext::create()->setGroups(array('Default', 'Detail'));
        $data = $this->getDoctrine()->getRepository('fraisBundle:FicheFrais')->find($id);
        $view = $this->view($data);
        $view->setSerializationContext($context);
        return $this->handleView($view);
    }

}
