<?php
/**
 * Created by PhpStorm.
 * User: sambeau
 * Date: 22/01/16
 * Time: 14:45
 */

namespace fraisBundle\Repository;


use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;

class FicheFraisRepository extends EntityRepository
{
    public function findFicheByMonth($month)
    {
        $query = $this->createQueryBuilder('ff')
            ->where('ff.mois = :month')
            ->setParameter('month', $month)
            ->getQuery();

        return $query->getResult();
    }

    public function findAllById(UserInterface $utilisateur)
    {
        $query = $this->createQueryBuilder('ff')
            ->join('ff.etat', 'ef')
            ->addSelect('ef')
            ->where('ff.utilisateur = :utilisateur')
            ->setParameter('utilisateur', $utilisateur->getId())
            ->orderBy('ff.utilisateur', 'ASC')
            ->getQuery();

        return $query->getResult();
    }


    public function findFicheFor($year, $mounth, UserInterface $user)
    {
        return $this->createQueryBuilder('ff')
            ->addSelect('lf, fhf, doc')
            ->leftJoin('ff.lesLignesForfaits', 'lf')
            ->leftJoin('ff.lesLignesHorsForfaits', 'fhf')
            ->leftJoin('ff.documents', 'doc')
            ->where('MONTH(ff.dateAction) = :mounth')
            ->andWhere('YEAR(ff.dateAction) = :year')
            ->andWhere('ff.utilisateur = :user')
            ->setParameter('mounth', $mounth)
            ->setParameter('year', $year)
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findListFicheOfYear(UserInterface $user)
    {
        $date = new \DateTime('-1 year');
        return $this->createQueryBuilder('ff')
            ->where('ff.dateAction > :date')
            ->andWhere('ff.utilisateur = :user')
            ->orderBy('ff.dateAction', 'DESC')
            ->setParameter('date', $date->format('Y-n-d'))
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getResult();
    }

    public function findListFicheForState($state)
    {
        return $this->createQueryBuilder('ff')
            ->addSelect('lf, fhf, doc')
            ->leftJoin('ff.lesLignesForfaits', 'lf')
            ->leftJoin('ff.lesLignesHorsForfaits', 'fhf')
            ->leftJoin('ff.documents', 'doc')
            ->where('ff.etat = :state')
            ->orderBy('ff.dateAction', 'DESC')
            ->setParameter('state', $state)
            ->getQuery()
            ->getResult();
    }

    public function findLastFiche(UserInterface $user)
    {
        return $this->createQueryBuilder('ff')
            ->orderBy('ff.dateAction', 'DESC')
            ->where('ff.utilisateur = :user')
            ->setMaxResults(1)
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }
}