<?php
/**
 * Created by PhpStorm.
 * User: sambeau
 * Date: 22/01/16
 * Time: 14:45
 */

namespace fraisBundle\Repository;


use Doctrine\ORM\EntityRepository;
use fraisBundle\Entity\Utilisateur;

class UtilisateurRepository extends EntityRepository
{
    public function getContactableComptable()
    {
        $qb = $this->createQueryBuilder('u');

        return $qb
            ->where('u.number IS NOT NULL')
            ->andWhere($qb->expr()->like('u.roles', ':role'))
            ->setParameter('role', '%' . Utilisateur::ROLE_COMPTABLE . '%')
            ->getQuery()
            ->getResult();
    }
}