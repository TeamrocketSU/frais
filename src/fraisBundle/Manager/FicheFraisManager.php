<?php
/**
 * Created by PhpStorm.
 * User: Allan
 * Date: 03/04/2016
 * Time: 09:29
 */

namespace fraisBundle\Manager;


use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use fraisBundle\Entity\FicheFrais;
use fraisBundle\Ref\EtatFiche;
use fraisBundle\Repository\FicheFraisRepository;

/**
 * Class FicheFraisManager
 * @package AppBundle\Manager
 */
class FicheFraisManager
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var FicheFraisRepository
     */
    private $repo;

    /**
     * @var int
     */
    private $dateFinSaisieFrais;

    /**
     * FicheFraisManager constructor.
     * @param EntityManager $manager
     * @param int $dateFinSaisieFrais
     */
    public function __construct(EntityManager $manager, $dateFinSaisieFrais)
    {
        $this->manager = $manager;
        $this->repo = $manager->getRepository('fraisBundle:FicheFrais');
        $this->dateFinSaisieFrais = $dateFinSaisieFrais;
    }

    /**
     * @param UserInterface $user
     * @return FicheFrais
     */
    public function getCurrentFiche(UserInterface $user)
    {
        $date = new \DateTime('now');
        if ($date->format('j') > $this->dateFinSaisieFrais) {
            $date->modify('+1 month');
        }

        $fiche = $this->getFicheFor($date->format('Y'), $date->format('n'), $user);
        if ($fiche == null) {
            $fiche = $this->getNewFiche($user, $date);
        }
        return $fiche;
    }

    /**
     * @param int $year
     * @param int $mounth
     * @param UserInterface $user
     * @return null|FicheFrais
     */
    public function getFicheFor($year, $mounth, UserInterface $user)
    {
        return $this->repo->findFicheFor($year, $mounth, $user);
    }

    /**
     * @param UserInterface $user
     * @param \DateTime $date
     * @return FicheFrais
     */
    public function getNewFiche(UserInterface $user, \DateTime $date)
    {
        return (new FicheFrais($user))
            ->setDateAction($date);
    }

    public function getListFicheSeekValid()
    {
        return $this->repo->findListFicheForState(EtatFiche::CLOSE);
    }

    public function getListFicheOfYear(UserInterface $user)
    {
        return $this->repo->findListFicheOfYear($user);
    }

    /**
     * @param UserInterface $user
     * @return null|FicheFrais
     */
    public function getLastFiche(UserInterface $user)
    {
        return $this->repo->findLastFiche($user);
    }

    /**
     * @param FicheFrais $ficheFrais
     * @return $this
     */
    public function save(FicheFrais $ficheFrais)
    {
        $this->manager->persist($ficheFrais);
        $this->manager->flush($ficheFrais);

        return $this;
    }

    /**
     * @return EntityManager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @return FicheFraisRepository
     */
    public function getRepo()
    {
        return $this->repo;
    }

}