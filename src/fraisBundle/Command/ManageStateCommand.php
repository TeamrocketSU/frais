<?php
/**
 * Created by PhpStorm.
 * User: Allan
 * Date: 10/04/2016
 * Time: 19:51
 */

namespace fraisBundle\Command;


use fraisBundle\Entity\FicheFrais;
use fraisBundle\Entity\LigneForfait;
use fraisBundle\Entity\LigneHorsForfait;
use fraisBundle\Ref\EtatFiche;
use fraisBundle\Ref\EtatLigne;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ManageStateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:manage_state')
            ->setDescription('Manage all state for fiche');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("CRON START");
        $manager = $this->getContainer()->get('frais.fiche_frais_manager');
        $now = new \DateTime('now');
        $day = $now->format('j');

        $dateCloture = $this->getContainer()->getParameter('date_cloture_fiche');
        $datePayement = $this->getContainer()->getParameter('date_mise_en_payement');
        $dateRembourser = $this->getContainer()->getParameter('date_rembourser');

        dump($datePayement);

        switch ($day) {
            case $dateCloture:
                /** @var FicheFrais $fiche */
                foreach ($manager->getRepo()->findListFicheForState(EtatFiche::CREATE) as $fiche) {
                    if ($fiche->getDateAction() < $now) {
                        $output->writeln("Cloture de : " . $fiche . " de : " . $fiche->getUtilisateur());
                        $fiche->setEtat(EtatFiche::CLOSE);
                    }
                }
                break;
            case $datePayement:
                /** @var FicheFrais $fiche */
                foreach ($manager->getRepo()->findListFicheForState(EtatFiche::CLOSE) as $fiche) {
                    $output->writeln(" ===== Finalisation de validation : " . $fiche . " de : " . $fiche->getUtilisateur());

                    /** @var LigneForfait|LigneHorsForfait $line */
                    foreach (array_merge(
                                 $fiche->getLesLignesForfaits()->toArray(),
                                 $fiche->getLesLignesHorsForfaits()->toArray()) as $line) {
                        if (false === in_array($line->getEtat(), array(
                                EtatLigne::VALID,
                                EtatLigne::WAIT_DOCUMENTS,
                            ))
                        ) {
                            $output->writeln("Ligne refusé : " . $line);
                            $line->setEtat(EtatLigne::REFUSE);
                        }
                    }

                    $output->writeln("Mise en payement de : " . $fiche . " de : " . $fiche->getUtilisateur());
                    $fiche->setEtat(EtatFiche::ON_PAYMENT);
                    $output->writeln(" ===== Fin de finalisation ===== ");

                }

                /** @var FicheFrais $fiche */
                foreach ($manager->getRepo()->findListFicheForState(EtatFiche::VALID) as $fiche) {
                    $output->writeln("Mise en payement de : " . $fiche . " de : " . $fiche->getUtilisateur());
                    $fiche->setEtat(EtatFiche::ON_PAYMENT);
                }
                break;

            case $dateRembourser:
                /** @var FicheFrais $fiche */
                foreach ($manager->getRepo()->findListFicheForState(EtatFiche::ON_PAYMENT) as $fiche) {
                    $output->writeln("Remboursement de : " . $fiche . " de : " . $fiche->getUtilisateur());
                    $fiche->setEtat(EtatFiche::REFUNDED);
                }
                break;
            default:
                $output->writeln("Aucune action ce jour. ");
        }
        $manager->getManager()->flush();
        $output->writeln("CRON END");
    }
}